﻿#include <iostream>

class Vector
{
private:
    double x = 0;
    double y = 0;
    double z = 0;
public:
    Vector()
    {}

    Vector(double _x, double _y, double _z): x(_x), y(_y), z(_z)
    {}

    double GetX() {
        return x;
    }

    double GetY() {
        return y;
    }

    double GetZ() {
        return z;
    }

    void SetX(double _x) {
        x = _x;
    }

    void SetY(double _y) {
        y = _y;
    }

    void SetZ(double _z) {
        z = _z;
    }

    void Update(double _x, double _y, double _z)
    {
        SetX(_x);
        SetY(_y);
        SetZ(_z);
    }

    void Print() {
        std::cout << "Vector {" << GetX() << ", " << GetY() << ", " << GetZ() << "}" << std::endl;
    }

    double Modulus() {
        double powSum = pow(GetX(), 2) + pow(GetY(), 2) + pow(GetZ(), 2);

        return sqrt(powSum);
    }
};

int main()
{
    Vector v;
    v.Print();
    std::cout << "Modulus " << v.Modulus() << std::endl;

    v.SetX(10);
    v.SetY(10);
    v.SetZ(5);
    v.Print();
    std::cout << "Modulus " << v.Modulus() << std::endl;

    v.Update(1, 2, 3);
    v.Print();
    std::cout << "Modulus " << v.Modulus() << std::endl;
}
